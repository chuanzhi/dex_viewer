﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace dex_viewer
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0 || !File.Exists(args[0]))
            {
                return;
            }
            try
            {
                string path = args[0];
                string libPath = AppDomain.CurrentDomain.BaseDirectory;
                // dex2jar
                var p = Process.Start(libPath + "/dex2jar-2.0/d2j-dex2jar.bat ", path);
                p.WaitForExit();
                // start jd-gui
                path = path.Substring(0, path.LastIndexOf(".")) + "-dex2jar.jar";
                Process.Start(libPath + "/jd-gui.exe ", path);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }
    }
}
